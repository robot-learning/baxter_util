# Baxter Utilities

This package contains various utility features for the Baxter research robot. This is intended to aggregate useful functionality specific to Baxter that perhaps doesn't require its own separate package.

## Current Functionality
### Button Listener for Demonstration Logging
Provides user control of logging robot data while performing kinesthetic demonstrations (moving the arm in Zero-G mode) through the button interfaces on the robot. The following capabilities are currently implemented:

* Start/stop logging of data (requires logging package; see below)
* Move to starting joint configuration (to initialize each demonstration to the same start)
* Reset initial starting joint configuration for demonstrations
* Zero out force/torque sensor (require Optoforce force/torque sensor and associated ROS driver; see below)

#### Dependencies
* Data logging is currently provided by the [LL4MA Logger](https://bitbucket.org/robot-learning/ll4ma_logger) package. The logger in that package provides services for saving recorded robot data to disk and clearing recorded data when it is to be discarded. It also handles the signals for starting and stopping logging in response to user button presses.
* Currently our lab has an Optoforce force sensor that uses the [optoforce_etherdaq](https://bitbucket.org/robot-learning/optoforce_etherdaq) ROS driver. If you would like to use the force sensor and be able to zero the sensor readings with a button press prior to performing a demonstration, you must have this package installed. It will advertise a `/zero_force_sensor` service that is associated with a Baxter button.