#!/usr/bin/env python
import os
import rospy
import rospkg
import cv2
import cv_bridge
import baxter_interface
import numpy as np
from std_msgs.msg import Bool
from std_srvs.srv import Empty
from sensor_msgs.msg import Image

# Allow for zeroing Optoforce force sensor if available
OPTOFORCE_FOUND = False
try:
    from optoforce_etherdaq_driver.srv import ZeroForceSensor
    OPTOFORCE_FOUND = True
except ImportError:
    pass

# log levels
INFO = 0
WARN = 1
ERROR = 2


class BaxterButtonListener:

    def __init__(self):
        self._log("Initializing...")

        # Retrieve ROS parameters from server
        self.limb = rospy.get_param("/baxter_button_listener/arm")
        self.robot_name = rospy.get_param("/baxter_button_listener/robot_name", "baxter")
        self.rate = rospy.Rate(rospy.get_param("/baxter_button_listener/rate", 100))
        if OPTOFORCE_FOUND:
            self.use_ft_sensor = rospy.get_param("/baxter_button_listener/use_ft_sensor")
        else:
            self.use_ft_sensor = False

        # ROS publishers
        self.button_state_pub = rospy.Publisher("/%s/button_state" % self.robot_name, Bool,
                                                queue_size=1)
        self.img_pub = rospy.Publisher("/robot/xdisplay", Image, latch=True, queue_size=1)

        # Setup image display for visual feedback on button press
        rospack = rospkg.RosPack()
        self.img_path = rospack.get_path("ll4ma_robot_control")
        self.img_path = os.path.join(self.img_path, "imgs")
        self.recording = False
        self.handle_demo = False
        
        # Try to initialize the arm
        attempts = 0
        arm_initialized = False
        while not arm_initialized and attempts < 5:
            try:
                self.arm = baxter_interface.Limb(self.limb)
                arm_initialized = True
                self._log("Arm initialized.")
            except OSError:
                attempts += 1
                self._log("Arm initialization failed. Trying again.", WARN)
        if not arm_initialized:
            self._log("Arm could not be initialized. Exiting.", ERROR)
            return
        else:
            self.init_joint_angles = self.arm.joint_angles()

        # Set head to face in general direction of user
        self.head = baxter_interface.Head()
        if self.limb == "left":
            self.head.set_pan(np.pi/4.0)
        else:
            self.head.set_pan(-np.pi/4.0)

        # Circle button for start/stop recording
        self.arm_circle_button = baxter_interface.DigitalIO("%s_button_ok" % self.limb)
        self.arm_circle_button.state_changed.connect(self._arm_circle_button_cb)
        # Arm back button for discarding recorded demonstration
        self.back_button = baxter_interface.DigitalIO("%s_button_back" % self.limb)
        self.back_button.state_changed.connect(self._arm_back_button_cb)
        # Rear shoulder button for moving arm to init at start of each demo
        self.rear_shoulder_button = baxter_interface.DigitalIO("%s_shoulder_button" % self.limb)
        self.rear_shoulder_button.state_changed.connect(self._rear_shoulder_button_cb)
        # Side torso button for setting current arm position as init for subsequent demos
        self.side_torso_button = baxter_interface.DigitalIO("torso_%s_button_ok" % self.limb)
        self.side_torso_button.state_changed.connect(self._side_torso_button_cb)
        # Rethink button for zero force sensor (if available)
        if self.use_ft_sensor:
            self.arm_rethink_button = baxter_interface.DigitalIO("%s_button_show" % self.limb)
            self.arm_rethink_button.state_changed.connect(self._arm_rethink_button_cb)
            self._log("Waiting for zero force service...")
            rospy.wait_for_service("zero_force_sensor")
            self._log("Zero force service found!")

        self._log("Waiting for logging services...")
        rospy.wait_for_service("clear_data")
        rospy.wait_for_service("save_data")
        self._log("Logging services found!")

        self._log("Initialization complete.")
        self._send_image("ready")

    def run(self):
        self._log("Listening for button presses...")
        while not rospy.is_shutdown():
            self.rate.sleep()


    def shutdown(self):
        self._log("Exiting.")
        self._send_image("researchsdk")
        self.head.set_pan(0.0)

    def _send_image(self, img_name):
        img_path = os.path.join(self.img_path, "%s.png" % img_name)
        img = cv2.imread(img_path)
        msg = cv_bridge.CvBridge().cv2_to_imgmsg(img, encoding="bgr8")
        self.img_pub.publish(msg)
        rospy.sleep(1.0)

    def _arm_circle_button_cb(self, down_press):
        """
        Circle button sends command to demonstration logger to start/stop logging data and
        to indicate in the affirmative that the demonstration should be saved to disk.
        """
        if down_press:
            if self.recording:
                self.button_state_pub.publish(True) # notify demo logger to stop
                self._send_image("keep")
                self.handle_demo = True
                self.recording = False
            elif self.handle_demo:
                success = False
                try:
                    save_data = rospy.ServiceProxy("save_data", Empty)
                    success = save_data()
                except rospy.ServiceException, e:
                    self._log("Something bad happened with /save_data service call: %s" % e, WARN)
                if success:
                    self._send_image("saved")
                    rospy.sleep(1.0)
                    self._send_image("ready")
                else:
                    self._send_image("error")
                self.handle_demo = False
            else:
                self.button_state_pub.publish(True) # notify button logger to start
                self._send_image("recording")
                self.recording = True

    def _arm_rethink_button_cb(self, down_press):
        """
        Rethink button makes service call to zero out force sensor. Only available if force sensor
        is indicated to be in use and Optoforce driver package was located on import.
        """
        if down_press and self.use_ft_sensor:
            success = False
            try:
                zero_force_sensor = rospy.ServiceProxy("zero_force_sensor", ZeroForceSensor)
                success = zero_force_sensor(True)
            except rospy.ServiceException, e:
                self._log("Something bad happened with /zero_force_sensor service call: %s" % e, WARN)
            if success:
                self._send_image("forces")
                rospy.sleep(1.0)
                self._send_image("ready")
                self._log("Force sensor zeroed.")

    def _arm_back_button_cb(self, down_press):
        """
        Back button indicates that recorded demonstration should not be saved to disk.
        """
        if down_press:
            if not self.recording and self.handle_demo:
                success = False
                try:
                    clear_data = rospy.ServiceProxy("clear_data", Empty)
                    success = clear_data()
                except rospy.ServiceException, e:
                    self._log("Something bad happened with /clear_data service call: %s" % e, WARN)
                if success:
                    self._send_image("discarded")
                    rospy.sleep(1.0)
                    self.handle_demo = False
                    self._send_image("ready")
                else:
                    self._send_image("error")

    def _rear_shoulder_button_cb(self, down_press):
        """
        Rear shoulder button commands the arm to move back to the initial position for demonstration.
        """
        if down_press:
            self.arm.move_to_joint_positions(self.init_joint_angles)

    def _side_torso_button_cb(self, down_press):
        """
        Side torso button sets the current position of the arm as the new starting point 
        for subsequent demonstrations.
        """
        if down_press:
            self.init_joint_angles = self.arm.joint_angles()
            self._send_image("init")
            rospy.sleep(1.0)
            self._send_image("ready")
            self._log("Initial joint angles set to current position.")

    def _log(self, msg, level=INFO):
        text = "[%s] %s" % (self.__class__.__name__, msg)
        if level == WARN:
            rospy.logwarn(text)
        elif level == ERROR:
            rospy.logerr(text)
        else:
            rospy.loginfo(text)
            

if __name__ == '__main__':
    rospy.init_node('baxter_button_listener')
    bbl = BaxterButtonListener()
    rospy.on_shutdown(bbl.shutdown)
    bbl.run()
